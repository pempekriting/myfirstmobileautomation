<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>profileMenu</name>
   <tag></tag>
   <elementGuidId>afdc6ae9-9bb5-4d32-b1d7-e9b843a5cb8e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//*[@class = 'android.widget.TextView' and (@text = 'Profile' or . = 'Profile')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
